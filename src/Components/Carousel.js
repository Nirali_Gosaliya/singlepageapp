import React from 'react';
import flower1 from '../assets/Images/flower1.jpg'
import flower2 from '../assets/Images/flower2.jpeg'
import flower3 from '../assets/Images/flower3.jpg'

function Bonus(props){
    return(
        <div>
            {/* carousel */}
            <div id="carousel-example-generic" className="carousel slide" data-ride="carousel">
  
  {/* <ol className="carousel-indicators">
    <li data-target="#carousel-example-generic" data-slide-to="0" className="active"></li>
    <li data-target="#carousel-example-generic" data-slide-to="1"></li>
    <li data-target="#carousel-example-generic" data-slide-to="2"></li>
  </ol> */}

  
  <div className="carousel-inner" role="listbox">
    <div className="item active">
      <img src={flower1} alt="flower1 image" className="flower_img" />
    </div>
    <div className="item">
      <img src={flower2} alt="flower2 image" className="flower_img" />
    </div>
    <div className="item">
      <img src={flower3} alt="flower3 image" className="flower_img" />
    </div>
    
  </div>

  {/* <!-- Controls --> */}
  <a className="left carousel-control" href="#carousel-example-generic" role="button" data-slide="prev">
    <span className="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
    <span className="sr-only">Previous</span>
  </a>
  <a className="right carousel-control" href="#carousel-example-generic" role="button" data-slide="next">
    <span className="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
    <span className="sr-only">Next</span>
  </a>
</div>

        </div>
    )
}

export default Bonus;