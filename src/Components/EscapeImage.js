import React from 'react'
import escape from '../assets/Images/escape.jpeg'

function EscapeImage() {
    return (
        <div>
            <div className='escape_parent'>
                <img src={escape} alt="Escape Image" className="escaped_image"></img>
                <div className='escape_plan'>
                    <h3 className='plan_inclusive_heading'>Plan Your All inclusive Escape</h3>
                    <p className='plan_inclusive_heading'>Discover inspiring destinations and learn why all inclusive is a great option for your next adventure</p>
                    <button type="button" className='btn btn-primary' className="plan_inclusive_heading" className="view_resorts">Explore All incusive Resorts</button>
                </div>
              
            </div>
        </div>
    )
}

export default EscapeImage;