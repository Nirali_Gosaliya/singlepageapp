import React from 'react'
import Envelop from './Envelop'

function EnvelopMenu() {
    return (
        <div>
            <div className='container-fluid'>
                <div className='row'>
                    <Envelop />
                </div>
             
            </div>
        </div>
    )
}

export default EnvelopMenu;