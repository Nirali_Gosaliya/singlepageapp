import React from "react";
import '../assets/CSS/Banner.css'
import Date from './Date'
import Buttons from './Buttons'
import Bonus from './Bonus'
import Carousel from './Carousel'
import ExploreSea from "./ExploreSea";

function Banner(){
    return(
        <div>
            <div className="container-fluid">
                <div className="row">
                    <div className="col-md-3">
                        <label className="where_to">Where to ?</label><br />
                        <input type="text" placeholder="City,state,location, or airport" className="input_field" className="form-control"></input>
                    </div>
                    <div className="col-md-1">
                       <Date date="3" month="March" day="THU" />
                    </div>
                    <div className="col-md-1">
                       <Date date="4" month="March" day="FRI" />
                    </div>
                    <div className="col-md-2">
                       <Buttons booking="1 Room, 1 Guest" />
                    </div>
                    <div className="col-md-2">
                       <Buttons booking="Special Rates" />

                    </div>
                    <div className="col-md-2">
                       <Buttons booking="Find a Hotel" />
                    </div>

                </div>
            </div>
            <div className="container-fluid">
                <div className="row">
                    <div className="col-md-12">
                        <Bonus heading="Get to free nights faster Earn 70k Bonus Points + a Free Night Reward.Terms apply." />
                    </div>
                    
                </div>
            </div>

            {/* carousel */}

            <div className="container-fluid">
                <div className="row">
                    <div className="col-md-12">
                         <Carousel />
                    </div>
                    
                </div>
            </div>

            {/* Explore the heart of the dead sea */}

            <div className="container-fluid">
                <div className="row">
                    <div className="col-md-12">
                        <ExploreSea />
                    </div>
                </div>
            </div>

        </div>
    )
}

export default Banner;