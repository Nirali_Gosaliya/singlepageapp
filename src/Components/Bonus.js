import React from 'react';

function Bonus(props){
    return(
        <div className="reward_info">
            <p className="reward">{props.heading}</p>
        </div>
    )
}

export default Bonus;