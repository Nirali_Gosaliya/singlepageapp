import React from 'react'

function SocialMediaHelp({ help_through_socialmedia }) {
    return (
        <div>
            <div className='help_of_socialmedia'>
                <p className='help_title'>{help_through_socialmedia.title}</p><br />
                <p className='helpline_no'>{help_through_socialmedia.helpline_no}</p>
                <p className='helpline_details'>{help_through_socialmedia.help_detail}</p>
            </div>

            {/* icons for help */}
            {/* <div className='icons'>
                <i className="fa fa-twitter-square fa-3x"  aria-hidden="true" style={{ color: "#1DA1F2" padding: 0 10px}}></i>
                <i className="fa fa-facebook-official fa-3x" aria-hidden="true" style={{ color: "#4267B2" }}></i>
                <i className="fa fa-instagram fa-3x" aria-hidden="true" style={{ color: "#FF0000" }}></i>
                <i className="fa fa-pinterest fa-3x" aria-hidden="true" style={{ color: "#E60023" }}></i>
            </div> */}

            <div>
                <ul>
                    <li><a href="/"><i className="fa fa-twitter-square fa-3x"  aria-hidden="true" style={{ color: "#1DA1F2"}}></i></a></li>
                    <li><a href='/'><i className="fa fa-facebook-official fa-3x" aria-hidden="true" style={{color: "#4267B2"}}></i></a></li>
                    <li><a href="/"><i className="fa fa-instagram fa-3x"  aria-hidden="true" style={{ color: "#FF0000"}}></i></a></li>
                    <li><a href="/"><i className="fa fa-youtube-play fa-3x" aria-hidden="true" style={{color: "#FF0000"}}></i></a></li>
                    <li><a href='/'><i className="fa fa-pinterest fa-3x" aria-hidden="true" style={{color: "#E60023"}}></i></a></li>
                    
                </ul>
            </div>

        </div>
    )
}

export default SocialMediaHelp;