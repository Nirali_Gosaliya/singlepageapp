import React from 'react';
import '../assets/CSS/NavBar.css'
import HiltonLogo from '../assets/Images/HiltonLogo.png'


function NavBar(){
    return(
        <div>
            <nav className="navbar">
            <div className="container-fluid">
   
    <div className="navbar-header">
      <button type="button" className="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
        <span className="sr-only">Toggle navigation</span>
        <span className="icon-bar"></span>
        <span className="icon-bar"></span>
        <span className="icon-bar"></span>
      </button>
      <div className='hiltonimg'>
          <img src={HiltonLogo} className="navbar-brand" alt="Hilton_logo_image" />
      </div>
      
    </div>

    
    <div className="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
      <ul className="nav navbar-nav">
        <li><a href="#">Locations</a></li>
        <li><a href="/">Offers</a></li>
        <li><a href="/">Meetings & Events</a></li>
        <li><a href="/">Resorts</a></li>
      </ul>
      
      <ul className="nav navbar-nav navbar-right">
        <li><a href="/">Join</a></li>
        <li><a href="/">Sign In</a></li>
      </ul>
    </div>
    <hr />
  </div>
</nav>
</div>
    )
}

export default NavBar;