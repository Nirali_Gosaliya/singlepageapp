import React from 'react';
import EscapeImage from './EscapeImage';

function InclusiveEscape() {
  return (
    <div>
        <div className="container-fluid">
            <div className='row'>
                <div className='col-md-12'>
                    <EscapeImage />
                </div>
            </div>
        </div>
    </div>
  )
}

export default InclusiveEscape;