import React from 'react';
import SocialMediaHelp from './SocialMediaHelp'
import LinksHelp from './LinksHelp';

function Footer() {
    let help_through_socialmedia = {
        title: "How can we Help?",
        helpline_no: "1-800-HILTONS",
        help_detail: "call us,It's toll free"
    }

    // Links 
    let help_through_links = {
       hilton_gift: "https://www.buyhiltongiftcards.com/",
        travel_inspiration: "https://www.earthtrekkers.com/travel-inspiration/",
        travel_career: "https://traveltriangle.com/blog/jobs-that-allow-you-to-travel/",
        tourism_development: "https://www.solimarinternational.com/what-we-do/tourism-development/",
        travel_data_resource: "https://data.world/datasets/travel",
        cookies: "https://www.kaspersky.com/resource-center/definitions/cookies",
        customer_support: "https://www.helpscout.com/helpu/definition-of-customer-support/",
        hilton_discount: "https://www.grabon.in/hilton-honors-coupons/"
    }
  return (
    <div>
        <div className="container-fluid">
            <div className='row'>
                <div className="col-md-6">
                    <SocialMediaHelp help_through_socialmedia={help_through_socialmedia} />
                </div>
                <div className='col-md-3'>
                    <LinksHelp help_through_links={help_through_links} />
                </div>
                <div className='col-md-3'>
                <LinksHelp help_through_links={help_through_links} />
                </div>
            </div>

        </div>
    </div>
  )
}

export default Footer;