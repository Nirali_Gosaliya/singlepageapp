import React from "react";

function Date(props){
    return(
        <div>
            <div>
             <h2 className="date">{props.date}</h2>
             <h5 className="month">
                <span className="month_1">{props.month}</span><br />
                <span className="day">{props.day}</span>
            </h5>
             
            </div>
           
        </div>
        
    )
}

export default Date;