import React,{useEffect,useState} from 'react'

function TravelMemories() {
    const [data,setData] = useState([]);

    useEffect(()=>{
        loadData();
    },[]);

    const loadData = async () =>{
        await fetch("./Data.json")
        .then(response => response.json())
        .then(receiveData => setData(receiveData))
    }
    console.log(data)
    return (
        <div>
            {/* Text */}
            <div>
                <h3>Explore SomeWhere New</h3>
                <p>Unlock new memories at our hotels around the world</p>
            </div>

            {/* Fetch method */}

           
            {data.map(user =>(
                    <div key={data.id} className="col-md-3">
                        <img src={user.url} alt="Traveling memories" className="travels_memory" />
                        
                    </div>
                ))}
           
        </div>


    )
}

export default TravelMemories;