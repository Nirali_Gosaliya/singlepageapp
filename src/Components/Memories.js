import React from 'react'
import TravelMemories from './TravelMemories';

function Memories() {
  return (
    <div>
        <div className="container-fuid">
            <div className='row'>
               
                    <TravelMemories />
                
            </div>
        </div>
    </div>
  )
}

export default Memories;