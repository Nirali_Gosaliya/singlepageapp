import React,{useState,useEffect} from 'react'

function Experience() {
    const [exp,setExp] = useState([]);
    useEffect(()=>{
        loadExp();
    },[])

    const loadExp = async ()=>{
        await fetch('./Exp.json')
        .then(response=>response.json())
        .then(receive =>setExp(receive))
    }
    console.log(exp);

  return (
    <div>
        <div>
        <h3>Experience Something New</h3>
        <p>Make your upcoming travel memorable with exciting experiences and offers from Hilton</p>
        </div>
        {/* fetch method */}
        {exp.map(userexp=>(
            <div key={userexp.id} className='col-md-4'>
                <img src={userexp.url} alt="Experience" className="experience_travel"></img>
                  
            </div>
        ))}
        
    </div>
  )
}

export default Experience;