import React from 'react'

function ExploreSea() {

    return (
        <div className="sea">
            <h3 className="explore_sea">Explore the Heart Of the Dead Sea</h3>
            <p className="hotel_specifications">Our hotel sits at the lowest land point on Earth, with direct access to the Dead sea beachfront.
                Enjoy our spa facilities,rooftop,bar,two infinity pools,and the area's only floating pontoon with sundeck.</p>
           
            
                <button type="button" className="view_button" className="btn btn-primary">View Hotel</button>
           
        </div>
    )
}

export default ExploreSea;