import React from 'react'

function LinksHelp({help_through_links}) {
  return (
    <div>
       <div>
           <ul className='links'>
               <li><a href="/">{help_through_links. hilton_gift}</a></li>
               <li><a href="/">{help_through_links. travel_inspiration}</a></li>
               <li><a href="/">{help_through_links. travel_career}</a></li>
               <li><a href="/">{help_through_links.tourism_development}</a></li>
               <li><a href="/">{help_through_links.  travel_data_resource}</a></li>
               <li><a href="/">{help_through_links.   cookies}</a></li>
               <li><a href="/">{help_through_links . customer_support}</a></li>
               <li><a href="/">{help_through_links . hilton_discount}</a></li>
              
           </ul>
       </div>
    </div>
  )
}

export default LinksHelp;