

import NavBar from './Components/NavBar'
import Banner from './Components/Banner';
import Memories from './Components/Memories';
import NewExperience from './Components/NewExperience';
import EnvelopMenu from './Components/EnvelopMenu';
import InclusiveEscape from './Components/InclusiveEscape';
import Footer from './Components/Footer';


function App() {
  return (
    <div>
     
     <NavBar />
     <Banner />
     <Memories />
     <NewExperience />
     <EnvelopMenu />
     <InclusiveEscape />
     <Footer />
    </div>
  );
}

export default App;
